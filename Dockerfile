######################
# MicroPython v1.9.4 #
######################

FROM python

RUN apt update -y && \
    mkdir esp8266 && \
    cd esp8266 && \
    pip3 install --upgrade setuptools && \
    pip3 install --upgrade wheel && \
    pip3 install    esptool \
                    adafruit-ampy && \
    wget http://micropython.org/resources/firmware/esp8266-20180511-v1.9.4.bin

WORKDIR esp8266/

ADD scripts/* /usr/local/bin/
RUN chmod +x /usr/local/bin/flash && \
    chmod +x /usr/local/bin/add && \
    chmod +x /usr/local/bin/list && \
    chmod +x /usr/local/bin/remove && \
    chmod +x /usr/local/bin/run

ENV PATH="$PATH:/usr/local/bin/flash" \
    PATH="$PATH:/usr/local/bin/add" \
    PATH="$PATH:/usr/local/bin/list" \
    PATH="$PATH:/usr/local/bin/remove" \
    PATH="$PATH:/usr/local/bin/run"

ENTRYPOINT [ "/bin/bash" ]
